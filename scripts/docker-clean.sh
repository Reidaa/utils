#!/usr/bin/env bash

set -x


docker container stop $(docker container list -aq)
docker container rm -f $(docker container list -aq)

docker image rm -f $(docker image list -aq)
