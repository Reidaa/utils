#
# Merging your kubeconfig file w/ $HOME/.kube/config (w/ cp backup)
#

set -x

cp $HOME/.kube/config $HOME/.kube/config.backup.$(date +%Y-%m-%d.%H:%M:%S)
KUBECONFIG=$HOME/.kube/config:$1 kubectl config view --merge --flatten > ~/.kube/merged_kubeconfig
mv ~/.kube/merged_kubeconfig ~/.kube/config
chmod 600 ~/.kube/config 
# kubectl get pods --context=cluster-1
# kubectl get pods --context=cluster-2
