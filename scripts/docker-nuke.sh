#!/usr/bin/env bash

set -x


docker container stop $(docker container list -aq)
docker container rm -f $(docker container list -aq)

docker image rm -f $(docker image list -aq)
docker volume rm -f $(docker volume list -q)

docker builder prune -f
